# Inventory with UMG

**这是一个实现了通过第一人称视角来拾取，使用，扔下物品功能的UE4工程**<br>
是UE4官方教程的一个搬运<br>
视频地址：https://www.youtube.com/playlist?list=PLZlv_N0_O1gZalvQWYs8sc7RP_-8eSr3i<br>


<p align="center">
<img src="https://gitlab.com/JingqingLin/inventoryWithUMG/raw/master/Pics/Outline.png" alt="The picture contains implementation process."/><br>
上图为实现此项目过程的大纲<br><br>
<img src="https://gitlab.com/JingqingLin/inventoryWithUMG/raw/master/Pics/UI.png" alt="The picture contains MenuUI."/><br>
上图为项目运行时的菜单界面<br><br>
<img src="https://gitlab.com/JingqingLin/inventoryWithUMG/raw/master/Pics/Setfire.png" alt="The picture contains setfire effect."/><br>
上图为项目运行时的燃火特效<br><br>
</p>